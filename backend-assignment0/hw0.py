from flask import Flask, jsonify, request, abort,Response
import hashlib
import simplejson as json
import logging
import ast
import time
import os
import io
import shutil
import requests
import binascii
import fileinput
from re import findall
from pymongo import MongoClient
from flask_pymongo import PyMongo
from werkzeug.datastructures import Headers
from collections import OrderedDict

app = Flask(__name__)
client = MongoClient('db', 27017)
# app.config['MONGO_URI'] = 'mongodb://chanin:backend0@ds261072.mlab.com:61072/backend_project0_mongo'
app.config['MONGO_URI'] = os.getenv("MONGO_URI", 'mongodb://localhost:27017/db')
# app.config['MONGO_URI'] = 'mongodb://localhost:27017/db'

mongo = PyMongo(app)
# db = client['bucket_db']

db = mongo.db

collectionslist = db.list_collection_names()

def update_collections():
    global collectionslist
    collectionslist = db.list_collection_names()
    return

print('initial collectionlists : ',collectionslist)

#for timestamp use int(time.time())

#=============================================================================================================================     

# def jsonDefault(OrderedDict):
#     return OrderedDict.__dict__

# #Classes note: probably getting depreciated soon, depreciated

# class bucket:
#     def __init__(self):
#         timeoncreate = int(time.time())
#         self.created = timeoncreate
#         self.modified = timeoncreate
#         self.list = []
#         self.name = ''

#     def __repr__(self):
#         return json.dumps(self,default=jsonDefault,indent=4)

#     def add_record_as_data(self,_record):
#         self.__dict__.update(_record.__dict__)

#     def add_record_as_attr(self,_record):
#         self.record = _record

# class object:
#     def __init__(self):
#         self.parts = dict()
#         self.meta = [] 

#     def __repr__(self):
#         return json.dumps(self,default=jsonDefault,indent=4)
        
#=============================================================================================================================     

#Routes

#bucket management route
@app.route('/<bucketname>',methods=['POST','GET','DELETE'])
def bucket_operations(bucketname):

    # Note: the name must be alphanumeric and may use _ or -
    for l in bucketname:
        if not (l.isalnum() or l == '_' or l == '-'):
            abort(400,'illegal bucket naming detected')
                
    #check the action
    print(request.args)       
    actions = list(request.args.to_dict().keys())
    print("--------------------------------------------------")
    print("REQUEST INFO: ")
    print(bucketname)
    action = str(actions[0])
    print(request.method)
    print(action)
    print("--------------------------------------------------")
    #how the f*ck do we get create only? UPDATE: IT'S JUST WORKS! STUPID BUT WORKS!
    if action == 'create' and request.method == 'POST':
        print("action : ",action)
        return create_bucket(bucketname)
    elif action == 'delete' and request.method == 'DELETE':
        print("action : ",action)
        return delete_bucket(bucketname)
    elif action == 'list' and request.method == 'GET':
        print("action : ",action)
        return list_bucket(bucketname)
    else:
        #this mean that the request does not contained an accepted action
        abort(400,'illegal method or method not recognised')

#object management route
@app.route('/<bucketname>/<objectname>',methods=['POST','GET','DELETE','PUT'])
def objects_operations(bucketname,objectname):
    
    #check first if specified bucket exists
    lib = list(db.list_collection_names())
    if bucketname in lib:
        print('----------------specified bucket ',bucketname,' found------------------')
        #check object name. (note : objectname must be alpha numeric and may contain . _ -, cannot start or end with .)
        objectnamelength = len(objectname)
        print(objectnamelength)
        for index in range(objectnamelength):
            if index == 0 or index == objectnamelength-1:
                if not (objectname[index].isalnum() or objectname[index] == '_' or objectname[index] == '-'):
                    abort(400,'illegal object naming')
            else:
                if not (objectname[index].isalnum() or objectname[index] == '_' or objectname[index] == '-' or objectname[index] == '.'):
                    abort(400,'illegal object naming')
        #check the args for action
        print(request.args)
        actions = list(request.args.to_dict().keys())
        action = ''
        print("--------------------------------------------------")
        print("REQUEST INFO: ")
        print(bucketname)
        print(objectname)
        if actions:
            action = str(actions[0])
        print(request.method)
        print(action)
        print("--------------------------------------------------")
        if action == "create" and request.method == 'POST':
            print("action : ",action)
            return create_upload_ticket(bucketname,objectname)
        elif action == "partNumber":
            print("action : ",action)
            #then differentiate between DELETE and UPLOAD(PUT)
            if request.method == 'PUT':
                return upload_all_parts(bucketname,objectname,request)
            elif request.method == 'DELETE':
                return delete_part(bucketname,objectname,request)
            else:
                abort(400,'invalid object method')
        elif action == "complete":
            print("action : ",action)
            return complete_multi_part_upload(bucketname,objectname)
        elif action == "delete" and request.method == 'DELETE':
            print("action : ",action)
            return delete_object(bucketname,objectname)
        elif action == "metadata":
            print("action : ",action)
            if request.method =='PUT':
                return update_meta(bucketname,objectname,request)
            elif request.method == 'DELETE':
                return delete_meta(bucketname,objectname,request)
            elif request.method == 'GET':
                return list_object_meta(bucketname,objectname,request)
            else:
                abort(404)
        elif (action==""):
            #Download
            if(request.method == 'GET'):
                print ("downloading : ", bucketname, " : ", objectname)
                return download_object(bucketname,objectname,request)
        else:
            abort(400,'illegal method or method not recognised')
    else:
        #bucket is not in list, abort
        abort(400,'bucket does not exist')

@app.route('/clearupobjects',methods=['DELETE'])
def delete_object_dir():

    if os.path.exists('/app/sos/objects'):
        shutil.rmtree('/app/sos/objects')

#=============================================================================================================================     

#Methods

#bucket methods

def create_bucket(bucketname):
    print('before : ',collectionslist)
    print("bucketname already exist? : ",(bucketname in collectionslist))
    if bucketname in collectionslist:
        print('----------------bucket already exist------------------')
        abort(400,'specified bucket already exists')
    else:
        print('----------------creating new bucket : ',bucketname,'-------------------')
        timeoncreate = int(time.time())
        # newbucket = bucket()
        # newbucket.name = bucketname
        # print(newbucket)
        
        newpath = '/app' + os.sep + 'sos' + os.sep + "buckets" + os.sep + '{}'.format(bucketname)
        try:
            os.makedirs(newpath)
        except OSError as error:
            print(error.message)
        # if not os.path.exists(newpath):
        #     os.makedirs(newpath)
        #     print('new folder made : ',bucketname)
        # else:
        #     abort(400,('directory with bucketname : ',bucketname,' already exists in the local directory'))
        bucket = db[bucketname]
        bucket.insert_one(({'name' : bucketname, 'created' : timeoncreate , 'modified' : timeoncreate, 'list' : [],'complete': [],'path': newpath}))
        first_entry = bucket.find_one({},{'_id':0,'name':1,'created':1,'modified':1})
        print('test printing : ',first_entry)
        print(type(first_entry))
        update_collections()
        print('after : ',collectionslist)
        return json.dumps(first_entry,indent=1)
        # return jsonify({'name' : bucketname, 'created' : timeoncreate , 'modified' : timeoncreate})

def delete_bucket(bucketname):
    if bucketname in collectionslist:
        print('before : ',collectionslist)
        print('----------------specified bucket ',bucketname,' found------------------')
        bucket = db[bucketname]
        path = bucket.find_one({},{'path':1})['path']
        if os.path.exists(path):
            try:
                shutil.rmtree(path)
            except OSError as e:
                print("Error: ",e.filename,e.strerror)
                abort(400,'cannot delete the directory from local directory')
        bucket.drop()
        update_collections()
        print('after : ',collectionslist)
        return jsonify({"deleted" : bucketname})
    else:
        print('----------------specified bucket does not exist------------------')
        abort(400,'specified bucket does not exists')

def list_bucket(bucketname):
    if bucketname in collectionslist:
        print('----------------bucket already exist------------------')
        bucket = db[bucketname]
        entries = bucket.find_one({},{'_id':0})
        path = bucket.find_one({},{'path':1})
        print('----------------------TESTING AREA--------------------------------')
        print(path)
        print(entries)
        print(type(entries))
        # for entry in entries['list']:
        #     print(entry['list'])
        print('----------------------TESTING AREA--------------------------------')
        return json.dumps(entries,indent=1)
    else:
        print('----------------specified bucket does not exist------------------')
        abort(400,'specified bucket does not exists')

#object methods

def create_upload_ticket(bucketname,objectname):
    requestbucket = db[bucketname]
    objectlist = requestbucket.find_one({},{'_id':0})['list']
    print(objectlist)
    if objectname in objectlist:
        abort(400,'objectname already exist in bucket')
    else:
        timemodified = int(time.time())
        bucketpath = requestbucket.find_one({},{'path':1})['path']
        if not os.path.exists(bucketpath):
            os.makedirs(bucketpath)
            # abort(400,'bucket directory does not exists on the machine local memory')
        oldvalues = requestbucket.find_one({},{'_id':0,'modified' : 1, 'list':1})
        objectlist.append(objectname)
        newvalues = ({"$set" :{'list' : objectlist,'modified' : timemodified}})
        requestbucket.update_one(oldvalues,newvalues)
        requestbucket.insert_one(({'objectname' : objectname, 'meta' : dict(),'completed_length':0,'completed_path':'','parts':[],'status':0}))
        afterupdate = db[bucketname].find()
        for item in afterupdate:
            print(item)
        # newobject = object()
        # requestbucket.list[objectname] = newobject
        return ("ticket created")

def upload_all_parts(bucketname,objectname,request):
    requestbucket = db[bucketname]
    objectlist = requestbucket.find_one({},{'id':0})['list']
    print(objectlist)
    if objectname in objectlist:
        timemodified = int(time.time())
        
        bucketpath = requestbucket.find_one({},{'path':1})['path']
        if not os.path.exists(bucketpath):
            os.makedirs(bucketpath)
            # abort(400,'bucket directory does not exists on the local machine')
        objectinfo = requestbucket.find_one({'objectname' : objectname},{'_id':0})
        objectstatus = objectinfo['status']
        partnumber = request.args.get('partNumber')
        if not (1<= int(partnumber) <=10000):
            abort(400,'Invalid partNumber (must be from 1 to 10000)')
        if(partnumber in objectinfo['parts']):
            abort(400,'specified partnumber already exists')
        if objectstatus == 1:
            abort(400,'the object is flagged as completed')
        else:
            header = request.headers
            print('headers : ',header)
            size = header.get('Content-Length')
            print('contentlength : ',size)
            md5fromheader = header.get('Content-MD5')
            uploaddata = request.data
            hash_md5 = hashlib.md5(uploaddata)
            print(md5fromheader)
            print(hash_md5.hexdigest())
            if md5fromheader != hash_md5.hexdigest():
                abort(400,'md5 mismatched')
            # if(int(size)>10000):
            #     abort(400,'uploaded file too large')
            # print(size)
            # partnumber = request.args.get('partNumber')
            f = open(bucketpath+ os.sep +objectname+'P'+partnumber,'wb')
            f.write(uploaddata)
            f.close()
            filesizeafter = os.path.getsize(bucketpath+ os.sep +objectname+'P'+partnumber)
            print('sizeatfer : ',filesizeafter)
            if int(filesizeafter) != int(size):
                abort(400,'content-length mismatched')
            print(type(uploaddata))
            uploaddata = request.get_data()
            requestbucket.insert_one({'partNumber' : partnumber,'object' : objectname, 'length' : size, 'md5': hash_md5.hexdigest(), 'path' : bucketpath+ os.sep +objectname+'P'+partnumber})
            oldparts = requestbucket.find_one({'objectname' : objectname},{'_id':0})['parts']
            print('before : ',oldparts)
            oldparts.append(partnumber)
            updatedinfo = ({'$set':{'parts' : oldparts}})
            requestbucket.update_one(objectinfo,updatedinfo)

            #change the modified time
            bucketinfo = requestbucket.find_one({},{'_id':0})
            newinfo = ({'$set':{'modified':timemodified}})
            requestbucket.update_one(bucketinfo,newinfo)
            result = requestbucket.find_one({'object' : objectname, 'partNumber' : partnumber},{'_id':0})
            # uploaded_md5 = hashlib.md5(datacontent)
            # print('after_md5 : ',uploaded_md5.hexdigest())
            return (json.dumps(result,indent=1))
    else:
        abort(400,'specified object does not exist in the bucket')

def download_object(bucketname,objectname,request):
    requestbucket = db[bucketname]
    objectlist = requestbucket.find_one({},{'id':0})['list']
    if(not objectname in objectlist):
        abort(404,'Object do not exist in the bucket')
    requestobject = requestbucket.find_one({'objectname':objectname},{'_id' : 0})
    if(requestobject['status'] == "0"):
        abort(400,'object not completed')
    totallength = requestobject['completed_length']

    headers = Headers()
    headers.add('Content-Disposition', 'attachment', filename=requestobject['objectname'])
    headers.add('Content-Transfer-Encoding','binary')

    # if not os.path.exists('.' + os.sep + 'objects'):
    #     os.makedirs('objects')
    # f = open('/app' + os.sep + 'objects' + os.sep + requestobject['objectname'],'wb')
    # f = io.BytesIO()
    # for part in sorted(requestobject['parts']):
    #     requestpart = requestbucket.find_one({'partNumber' : part,'object' : objectname},{'_id':0})
    #     try:
    #         openfile = open(requestpart['path'],'rb')
    #         binaryreaded = openfile.read()
    #         # print(binaryreaded[:10])
    #         loopedlength += len(binaryreaded)
    #         f.write(binaryreaded)
    #         # f.write((partdata))
    #         openfile.close()
    #     except OSError:
    #         abort(400,'file does not exist on local : '+requestpart['path'])
        # r.close()
        # f.write((requestpart['datacontent']))
    # f.close()
    
    f = open(requestobject['completed_path'],'rb')

    status = 200
    size   = totallength
    if size<=0:
        abort(400,'what the fuck? how did we reach this one?')
    begin  = 0;
    end    = size-1;

    if request.headers.has_key("Range") and rangerequest:
        status = 206
        headers.add('Accept-Ranges','bytes')
        ranges = findall(r"\d+", request.headers["Range"])
        begin  = int( ranges[0] )
        if len(ranges)>1:
            end = int( ranges[1] )
        headers.add('Content-Range','bytes %s-%s/%s' % (str(begin),str(end),str(end-begin)) )

    headers.add('Content-Length',str((end-begin)+1))
    response = Response(f, status=status, headers=headers, direct_passthrough=True)
    # f.close()
    return response


def complete_multi_part_upload(bucketname,objectname):
    requestbucket = db[bucketname]
    objectlist = requestbucket.find_one({},{'id':0})['list']
    completionlist = requestbucket.find_one({},{'id':0})['complete']
    if (objectname in completionlist):
        abort(400,'object is already completed')
    if (objectname in objectlist):
        timemodified = int(time.time())
        oldinfo = requestbucket.find_one({'objectname' : objectname},{'_id':0})
        oldinfoparts = requestbucket.find_one({'objectname' : objectname},{'_id':0})['parts']
        oldinfoparts.sort()
        # etagformd5 = ''
        if not os.path.exists('/app' + os.sep + 'sos' + os.sep + 'objects'):
            try:
                os.makedirs('/app' + os.sep + 'sos' + os.sep + 'objects')
            except OSError as error:
                print(error.message)
                abort(400,'error making folder wtf?')
        objectfilepath = '/app' + os.sep + 'sos' + os.sep + 'objects' + os.sep + '{}'.format(objectname)
        f = open(objectfilepath,'wb')
        totalsize = 0
        totalparts = len(oldinfoparts)
        for part in oldinfoparts:
            requestpart = requestbucket.find_one({'partNumber' : part,'object' : objectname},{'_id' : 0})
            try:
                r = open(requestpart['path'],'rb')
                f.write(r.read())
                r.close()
            except OSError:
                abort(400,'error writing part')
            # etagformd5+=requestpart['md5']
            totalsize += int(requestpart['length'])
        # complete_md5 = hashlib.md5(etagformd5)
        f.close()
        f = open(objectfilepath,'rb')
        thechunk = f.read()
        complete_md5 = hashlib.md5(thechunk)
        f.close()
        sendhelp =  str(complete_md5.hexdigest())+('-'+str(totalparts))
        newinfo = ({'$set' :{'meta': oldinfo['meta'],'status' : 1,'parts':oldinfo['parts'],'completed_path' : objectfilepath,'completed_length' : totalsize}})
        requestbucket.update_one(oldinfo,newinfo)
        afterobject = requestbucket.find_one({'objectname': objectname},{'_id':0})
        print(afterobject)
        bucketinfo = requestbucket.find_one({},{'_id':0})
        print('before : ',bucketinfo)
        completionlist.append(objectname)
        updatedbucketinfo = ({'$set':{'complete':completionlist,'modified': timemodified}})
        requestbucket.update_one(bucketinfo,updatedbucketinfo)
        updated = requestbucket.find_one({},{'_id':0})
        print('updated : ',updated)
        return(jsonify({'etag' : sendhelp}))

def delete_part(bucketname,objectname,request):
    requestbucket = db[bucketname]
    objectlist = requestbucket.find_one({},{'_id':0})['list']
    if (objectname in objectlist):
        requestobject = requestbucket.find_one({'objectname' : objectname},{'_id':0})
        partslist = requestbucket.find_one({'objectname' : objectname},{'_id':0})['parts']
        partnumber = request.args.get('partNumber')
        if partnumber in partslist:
            timemodified = int(time.time())
            specifiedpart = requestbucket.find_one({'partNumber': partnumber, 'object' : objectname},{'_id':0})
            if os.path.isfile(specifiedpart['path']):
                try:
                    os.remove(specifiedpart['path'])
                except OSError as e:
                    print("Error: ",e.filename,e.strerror)
                    abort(500,'cannot delete the file from local directory')
            requestbucket.delete_one(specifiedpart)
            print(partslist)
            partslist.remove(partnumber)
            print(partslist)
            newinfo = ({'$set' : {'parts' : partslist}})
            print(requestobject)
            requestbucket.update_one(requestobject,newinfo)
            updated = requestbucket.find_one({'objectname' : objectname},{'_id':0})
            print('updated : ',updated)
            bucketinfo = requestbucket.find_one({},{'_id':0})
            updatedbucketinfo = ({'$set':{'modified': timemodified}})
            requestbucket.update_one(bucketinfo,updatedbucketinfo)
            print('updated : ',updated)
            return('deleted : ',objectname,' ',partnumber)
        else:
            abort(400,'specified part does not exists')
    else:
        abort(400,'specified object does not exists')

def delete_object(bucketname,objectname):
    requestbucket = db[bucketname]
    bucketinfo = requestbucket.find_one({},{'_id':0})
    objectlist = requestbucket.find_one({},{'_id':0})['list']
    completelist = requestbucket.find_one({},{'_id':0})['complete']
    if objectname in objectlist:
        timemodified = int(time.time())
        deleting_object = requestbucket.find_one({'objectname' : objectname},{'_id':0})
        deleting_object_parts = requestbucket.find_one({'objectname' : objectname},{'_id':0})['parts']
        for part in deleting_object_parts:
            deleting_part = requestbucket.find_one({'partNumber' : part,'object' : objectname},{'_id':0})
            if os.path.isfile(deleting_part['path']):
                try:
                    os.remove(deleting_part['path'])
                except OSError as e:
                    print("Error: ",e.filename,e.strerror)
                    abort(400,'cannot delete the file from local directory')
            print('deleting object : ',objectname,' part : ',part)
            requestbucket.delete_one(deleting_part)
        if(deleting_object['status'] == '1'):
            try:
                os.remove(deleting_object['completed_path'])
            except OSError as e:
                abort(400,'cannot delete completed file from directory')
        if objectname in completelist:
            completelist.remove(objectname)
        requestbucket.delete_one(deleting_object)
        objectlist.remove(objectname)
        updatedbucketinfo = ({'$set':{'complete' : completelist,'modified' : timemodified, 'list' : objectlist}})
        requestbucket.update_one(bucketinfo,updatedbucketinfo)
        print(requestbucket.find_one({},{'_id':0}))
        return ('deleted : ', objectname)
    else:
        abort(400,'object does not exists in the specified bucket')
        
def update_meta(bucketname,objectname,request):
    requestbucket = db[bucketname]
    bucketinfo = requestbucket.find_one({},{'_id':0})
    objectlist = requestbucket.find_one({},{'_id':0})['list']
    if objectname in objectlist:
        timemodified = int(time.time())
        inputkey = request.args.get('key')
        print(inputkey)
        requestobject = requestbucket.find_one({'objectname' : objectname},{'_id':0})
        print(requestobject)
        requestobjectmeta = requestbucket.find_one({'objectname' : objectname},{'_id':0})['meta']
        uploadedmeta = request.get_data()
        # print('bro')
        # print(uploadedmeta)
        if not uploadedmeta:
            # print('was here')
            abort(400)
        else:
            print('actually in here')
            requestobjectmeta[inputkey] = uploadedmeta
            updatedobject = ({'$set' : {'meta' : requestobject}})
            requestbucket.update_one(requestobject,updatedobject)
            #testing
            afterupdated = requestbucket.find_one({'objectname' : objectname},{'_id':0})
            print(afterupdated)
            updatedbucket = ({'$set' : {'modified' : timemodified}})
            requestbucket.update_one(bucketinfo,updatedbucket)
            return('updated_meta : ',objectname)
    else:
        abort(404)

def delete_meta(bucketname,objectname,request):
    requestbucket = db[bucketname]     
    bucketinfo = requestbucket.find_one({},{'_id':0})
    objectlist = requestbucket.find_one({},{'_id':0})['list']
    if objectname in objectlist:
        timemodified = int(time.time())
        inputkey = requestbucket.args.get('key')
        print(inputkey)
        requestobject = requestbucket.find_one({'objectname' : objectname},{'_id':0})
        requestobjectmeta = requestbucket.find_one({'objectname' : objectname},{'_id':0})['meta']
        if inputkey in requestobjectmeta:
            del requestobjectmeta[inputkey]
            updatedobjectinfo = ({'$set' : {'meta' : requestobjectmeta}})
            requestbucket.update_one(requestobject,updatedobjectinfo)
            print(requestbucket.find_one({'objectname' : objectname},{'_id':0}))
            updatedbucket = ({'$set' : {'modified' : timemodified}})
            requestbucket.update_one(bucketinfo,updatedbucket)
            return('deleted_meta : ',objectname,' ',inputkey)
        else:
            abort(404)
    else:
        abort(404)

def list_object_meta(bucketname,objectname,request):
    requestbucket = db[bucketname]     
    objectlist = requestbucket.find_one({},{'_id':0})['list']
    if objectname in objectlist:
        inputkey = requestbucket.args.get('key')
        print(inputkey)
        requestobjectmeta = requestbucket.find_one({'objectname' : objectname},{'_id':0})['meta']
        return json.dumps(requestobjectmeta)
    else:
        abort(404)

#=============================================================================================================================     

if __name__ == "__main__":
    app.run(debug=True)