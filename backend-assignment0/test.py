from hw0 import bucket_operations
import requests

BASE_URL = 'http://127.0.0.1:5000'
STATUS_OK = requests.codes['ok']
BAD_RESPONSE = requests.codes['bad_request']

#'test creating a new bucket named 5781033'
def test_bucket_create():
    resp = requests.post(BASE_URL + '/5781033?create')
    assert resp.status_code == STATUS_OK

# #'test creating bucket under the same name'
def test_duplicate_bucket_create():
    resp = requests.post(BASE_URL + '/5781033?create')
    assert resp.status_code == BAD_RESPONSE

#'test listing existing bucket'
def test_list_bucket():
    resp = requests.get(BASE_URL + '/5781033?list')
    assert resp.status_code == STATUS_OK

#'test deleting after created'
def test_bucket_delete():
    resp = requests.delete(BASE_URL + '/5781033?delete')
    assert resp.status_code == STATUS_OK

#'test deleting a not existing bucket'
def test_nonexist_bucket_delete():
    resp = requests.delete(BASE_URL + '/5781033?delete')
    assert resp.status_code == BAD_RESPONSE

#'test listing existing bucket'
def test_list_nonexist_bucket():
    resp = requests.get(BASE_URL + '/5781033?list')
    assert resp.status_code == BAD_RESPONSE

#'test non exist action'
def test_nonexist_action():
    resp = requests.get(BASE_URL + '/5781033?meh')
    assert resp.status_code == BAD_RESPONSE

#test illegal bucket naming
def test_illegal_bucket():
    resp = requests.post(BASE_URL + '/578+1033?create')
    assert resp.status_code == BAD_RESPONSE

#test method not allow